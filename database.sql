-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: test5
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `championship`
--

DROP TABLE IF EXISTS `championship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `championship` (
  `id` char(32) NOT NULL COMMENT 'идентификатор',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'дата\\время создания',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='чемпионат';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `championship`
--

LOCK TABLES `championship` WRITE;
/*!40000 ALTER TABLE `championship` DISABLE KEYS */;
INSERT INTO `championship` VALUES ('8153f43ef21eaecc3ef6c403429c9bc8','2017-10-12 03:55:05');
/*!40000 ALTER TABLE `championship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competition`
--

DROP TABLE IF EXISTS `competition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competition` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` char(32) DEFAULT NULL COMMENT 'идентификатор родительской группы',
  `championship_id` char(32) NOT NULL COMMENT 'идентификатор сессии',
  `group_id` char(32) NOT NULL COMMENT 'идентификатор группы',
  `teamA_id` tinyint(3) unsigned NOT NULL COMMENT 'индетификатор первой комманды',
  `teamB_id` tinyint(3) unsigned NOT NULL COMMENT 'индетификатор второй комманды',
  `teamA_goals` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'голы команды "A" в розыгрыше',
  `teamB_goals` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'голы команды "B" в розыгрыше',
  `teamA_goals_total` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'всего голов команды "A"',
  `teamB_goals_total` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'всего голов команды "B"',
  `teamA_scores` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'очки команды "A"',
  `teamB_scores` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'очки команды "B"',
  `played` datetime DEFAULT NULL COMMENT 'дата\\время игры',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'дата\\время создания',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_idx` (`group_id`,`teamA_id`,`teamB_id`) USING BTREE,
  KEY `idx_team_id` (`teamA_id`,`teamB_id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `fk_competition_teamB_id_team_idx` (`teamB_id`),
  KEY `fk_competition_championship_id_championship` (`championship_id`),
  CONSTRAINT `fk_competition_championship_id_championship` FOREIGN KEY (`championship_id`) REFERENCES `championship` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_competition_teamA_id_team` FOREIGN KEY (`teamA_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_competition_teamB_id_team` FOREIGN KEY (`teamB_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1717 DEFAULT CHARSET=utf8mb4 COMMENT='соревнование';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competition`
--

LOCK TABLES `competition` WRITE;
/*!40000 ALTER TABLE `competition` DISABLE KEYS */;
INSERT INTO `competition` VALUES (1701,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','1a54ce173cf24dd9f9e88fdeab338b18',2,16,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1702,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','1a54ce173cf24dd9f9e88fdeab338b18',27,31,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1703,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','79642452113e58042b549a5adf17ab0f',17,9,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1704,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','79642452113e58042b549a5adf17ab0f',8,29,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1705,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','6bf5ce3d57bd50c35e9aa9301c6db3b3',28,15,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1706,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','6bf5ce3d57bd50c35e9aa9301c6db3b3',18,12,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1707,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','90bc8b65e8fd473f3f44c4ef6e48bb63',23,30,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1708,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','90bc8b65e8fd473f3f44c4ef6e48bb63',21,1,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1709,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','7d5b3067cb9d617e00c7a8a7bfef4d00',4,14,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1710,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','7d5b3067cb9d617e00c7a8a7bfef4d00',26,10,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1711,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','853fd4837c29cd8470a7ec653bf823fb',20,13,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1712,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','853fd4837c29cd8470a7ec653bf823fb',24,5,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1713,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','d019df8e3a36a5de350ba2af4c5bb336',7,25,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1714,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','d019df8e3a36a5de350ba2af4c5bb336',6,11,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1715,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','d6f29c69b7051405cd0081552e429f11',22,32,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1716,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','d6f29c69b7051405cd0081552e429f11',19,3,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06');
/*!40000 ALTER TABLE `competition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` tinyint(3) unsigned NOT NULL COMMENT 'идентификатор',
  `country` varchar(20) NOT NULL COMMENT 'страна',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='команда';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'Бразилия'),(2,'Германия / ФРГ'),(3,'Италия'),(4,'Аргентина'),(5,'Англия'),(6,'Испания'),(7,'Франция'),(8,'Голландия'),(9,'Уругвай'),(10,'Швеция'),(11,'Россия / СССР'),(12,'Сербия / Югославия'),(13,'Мексика'),(14,'Бельгия'),(15,'Польша'),(16,'Венгрия'),(17,'Португалия'),(18,'Чехия / Чехословакия'),(19,'Чили'),(20,'Австрия'),(21,'Швейцария'),(22,'Парагвай'),(23,'США'),(24,'Румыния'),(25,'Южная Корея'),(26,'Дания'),(27,'Хорватия'),(28,'Колумбия'),(29,'Шотландия'),(30,'Камерун'),(31,'Коста-Рика'),(32,'Болгария'),(33,'Нигерия'),(34,'Ирландия'),(35,'Япония'),(36,'Турция'),(37,'Гана'),(38,'Сев. Ирландия'),(39,'Перу'),(40,'Эквадор'),(41,'Алжир'),(42,'ЮАР'),(43,'Марокко'),(44,'Кот-д\'Ивуар'),(45,'Норвегия'),(46,'Австралия'),(47,'Сенегал'),(48,'ГДР'),(49,'Тунис'),(50,'Греция'),(51,'Саудовская Аравия'),(52,'Уэльс'),(53,'Украина'),(54,'Иран'),(55,'Словакия'),(56,'Словения'),(57,'Куба'),(58,'Новая Зеландия'),(59,'Гондурас'),(60,'КНДР'),(61,'Босния и Герцеговина'),(62,'Ангола'),(63,'Израиль'),(64,'Египет'),(65,'Ямайка'),(66,'Кувейт'),(67,'Тринидад и Тобаго'),(68,'Боливия'),(69,'Ирак'),(70,'Того'),(71,'Канада'),(72,'Гол. Вест-Индия'),(73,'ОАЭ'),(74,'Китай'),(75,'Гаити'),(76,'Заир'),(77,'Сальвадор');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_history`
--

DROP TABLE IF EXISTS `team_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_history` (
  `team_id` tinyint(3) unsigned NOT NULL COMMENT 'идентификатор',
  `parts` tinyint(3) unsigned NOT NULL COMMENT 'кол-во участий в турнире',
  `games` tinyint(3) unsigned NOT NULL COMMENT 'кол-во сыгранных игр',
  `wins` tinyint(3) unsigned NOT NULL COMMENT 'побед',
  `draws` tinyint(3) unsigned NOT NULL COMMENT 'ничей',
  `defeats` tinyint(3) unsigned NOT NULL COMMENT 'поражений',
  `scored` tinyint(3) unsigned NOT NULL COMMENT 'забито',
  `missed` tinyint(3) unsigned NOT NULL COMMENT 'пропущено',
  `scores` tinyint(3) unsigned NOT NULL COMMENT 'кол-во очков',
  `percent` tinyint(2) unsigned NOT NULL,
  `firsts` tinyint(3) unsigned NOT NULL COMMENT 'сколько раз были первыми',
  `seconds` tinyint(3) unsigned NOT NULL COMMENT 'сколько раз были вторыми',
  `thirds` tinyint(3) unsigned NOT NULL COMMENT 'сколько раз были третьими',
  PRIMARY KEY (`team_id`),
  CONSTRAINT `fk_team_history_team_id_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='исторические данные команды';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_history`
--

LOCK TABLES `team_history` WRITE;
/*!40000 ALTER TABLE `team_history` DISABLE KEYS */;
INSERT INTO `team_history` VALUES (1,20,104,70,17,17,127,102,157,76,5,2,2),(2,18,106,66,20,20,127,121,152,72,4,4,4),(3,18,83,45,21,17,127,77,111,67,4,2,1),(4,16,77,42,14,21,127,84,98,64,2,3,0),(5,14,62,26,20,16,79,56,72,58,1,0,0),(6,14,59,29,12,18,92,66,70,59,1,0,0),(7,14,59,28,12,19,106,71,68,58,1,1,2),(8,10,50,27,12,11,86,48,66,66,0,3,1),(9,12,51,20,12,19,80,71,52,51,2,0,0),(10,11,46,16,13,17,74,69,45,49,0,1,2),(11,10,40,17,8,15,66,47,42,53,0,0,0),(12,11,43,17,8,18,64,59,42,49,0,0,0),(13,15,53,14,14,25,57,92,42,40,0,0,0),(14,12,41,14,9,18,52,66,37,45,0,0,0),(15,7,31,15,5,11,44,40,35,57,0,0,2),(16,9,32,15,3,14,87,57,33,52,0,2,0),(17,6,26,13,4,9,43,29,30,58,0,0,1),(18,9,33,12,5,16,47,49,29,44,0,2,0),(19,9,33,11,7,15,40,49,29,44,0,0,1),(20,7,29,12,4,13,43,47,28,48,0,0,1),(21,10,33,11,6,16,45,59,28,42,0,0,0),(22,8,27,7,10,10,30,38,24,44,0,0,0),(23,10,33,8,6,19,37,62,22,33,0,0,1),(24,7,21,8,5,8,30,32,21,50,0,0,0),(25,9,31,5,9,17,31,67,19,31,0,0,0),(26,4,16,8,2,6,27,24,18,56,0,0,0),(27,4,16,7,2,7,21,17,16,50,0,0,1),(28,5,18,7,2,9,26,27,16,44,0,0,0),(29,8,23,4,7,12,25,41,15,33,0,0,0),(30,7,23,4,7,12,18,43,15,33,0,0,0),(31,4,15,5,4,6,17,23,14,47,0,0,0),(32,7,26,3,8,15,22,53,14,27,0,0,0),(33,5,18,5,3,10,20,26,13,36,0,0,0),(34,3,13,2,8,3,10,10,12,46,0,0,0),(35,5,17,4,4,9,14,22,12,35,0,0,0),(36,2,10,5,1,4,20,17,11,55,0,0,1),(37,3,12,4,3,5,13,16,11,46,0,0,0),(38,3,13,3,5,5,13,23,11,42,0,0,0),(39,4,15,4,3,8,19,31,11,37,0,0,0),(40,3,10,4,1,5,10,11,9,45,0,0,0),(41,4,13,3,3,7,13,19,9,35,0,0,0),(42,3,9,2,4,3,11,16,8,44,0,0,0),(43,4,13,2,4,7,12,18,8,31,0,0,0),(44,3,9,3,1,5,13,14,7,39,0,0,0),(45,3,8,2,3,3,7,8,7,44,0,0,0),(46,4,13,2,3,8,11,26,7,27,0,0,0),(47,1,5,2,2,1,7,6,6,60,0,0,0),(48,1,6,2,2,2,5,5,6,50,0,0,0),(49,4,12,1,4,7,8,17,6,25,0,0,0),(50,3,10,2,2,6,5,20,6,30,0,0,0),(51,4,13,2,2,9,9,32,6,23,0,0,0),(52,1,5,1,3,1,4,4,5,50,0,0,0),(53,1,5,2,1,2,5,7,5,50,0,0,0),(54,4,12,1,3,8,7,22,5,21,0,0,0),(55,1,4,1,1,2,5,7,3,38,0,0,0),(56,2,6,1,1,4,5,10,3,25,0,0,0),(57,1,3,1,1,1,5,12,3,50,0,0,0),(58,2,6,0,3,3,4,14,3,25,0,0,0),(59,3,9,0,3,6,3,14,3,17,0,0,0),(60,2,7,1,1,5,6,21,3,21,0,0,0),(61,1,3,1,0,2,4,4,2,33,0,0,0),(62,1,3,0,2,1,1,2,2,33,0,0,0),(63,1,3,0,2,1,1,3,2,33,0,0,0),(64,2,4,0,2,2,3,6,2,25,0,0,0),(65,1,3,1,0,2,3,9,2,33,0,0,0),(66,1,3,0,1,2,2,6,1,17,0,0,0),(67,1,3,0,1,2,0,4,1,17,0,0,0),(68,3,6,0,1,5,1,20,1,8,0,0,0),(69,1,3,0,0,3,1,4,0,0,0,0,0),(70,1,3,0,0,3,1,6,0,0,0,0,0),(71,1,3,0,0,3,0,5,0,0,0,0,0),(72,1,1,0,0,1,0,6,0,0,0,0,0),(73,1,3,0,0,3,2,11,0,0,0,0,0),(74,1,3,0,0,3,0,9,0,0,0,0,0),(75,1,3,0,0,3,2,14,0,0,0,0,0),(76,1,3,0,0,3,0,14,0,0,0,0,0),(77,2,6,0,0,6,1,22,0,0,0,0,0);
/*!40000 ALTER TABLE `team_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_championship`
--

DROP TABLE IF EXISTS `v_championship`;
/*!50001 DROP VIEW IF EXISTS `v_championship`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_championship` AS SELECT 
 1 AS `championship_id`,
 1 AS `group_id`,
 1 AS `parent_id`,
 1 AS `played`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_competition`
--

DROP TABLE IF EXISTS `v_competition`;
/*!50001 DROP VIEW IF EXISTS `v_competition`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_competition` AS SELECT 
 1 AS `team_id`,
 1 AS `games`,
 1 AS `scored`,
 1 AS `missed`,
 1 AS `scores`,
 1 AS `wins`,
 1 AS `draws`,
 1 AS `defeats`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_group`
--

DROP TABLE IF EXISTS `v_group`;
/*!50001 DROP VIEW IF EXISTS `v_group`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_group` AS SELECT 
 1 AS `group_id`,
 1 AS `played`,
 1 AS `teamA_id`,
 1 AS `teamA_country`,
 1 AS `teamA_goals`,
 1 AS `teamA_scored_missed`,
 1 AS `teamB_id`,
 1 AS `teamB_country`,
 1 AS `teamB_goals`,
 1 AS `teamB_scored_missed`,
 1 AS `teamA_scores`,
 1 AS `teamB_scores`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_group_winner`
--

DROP TABLE IF EXISTS `v_group_winner`;
/*!50001 DROP VIEW IF EXISTS `v_group_winner`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_group_winner` AS SELECT 
 1 AS `group_id`,
 1 AS `teamA_country`,
 1 AS `teamA_goals`,
 1 AS `teamA_id`,
 1 AS `teamB_id`,
 1 AS `teamB_country`,
 1 AS `teamB_goals`,
 1 AS `teamA_scores`,
 1 AS `teamB_scores`,
 1 AS `teamA_scored_missed`,
 1 AS `teamB_scored_missed`,
 1 AS `teamA_scores_total`,
 1 AS `teamB_scores_total`,
 1 AS `winner_letter`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_team`
--

DROP TABLE IF EXISTS `v_team`;
/*!50001 DROP VIEW IF EXISTS `v_team`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_team` AS SELECT 
 1 AS `id`,
 1 AS `country`,
 1 AS `team_id`,
 1 AS `games`,
 1 AS `scored`,
 1 AS `missed`,
 1 AS `scored_missed`,
 1 AS `scores`,
 1 AS `wins`,
 1 AS `draws`,
 1 AS `defeats`,
 1 AS `attack_power`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_team_history`
--

DROP TABLE IF EXISTS `v_team_history`;
/*!50001 DROP VIEW IF EXISTS `v_team_history`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_team_history` AS SELECT 
 1 AS `team_id`,
 1 AS `games`,
 1 AS `scored`,
 1 AS `missed`,
 1 AS `scores`,
 1 AS `wins`,
 1 AS `draws`,
 1 AS `defeats`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'test5'
--

--
-- Final view structure for view `v_championship`
--

/*!50001 DROP VIEW IF EXISTS `v_championship`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_championship` AS select sql_small_result `c1`.`championship_id` AS `championship_id`,`c1`.`group_id` AS `group_id`,`c1`.`parent_id` AS `parent_id`,`c1`.`played` AS `played` from `competition` `c1` where (`c1`.`played` is not null) group by `c1`.`championship_id`,`c1`.`group_id`,`c1`.`parent_id`,`c1`.`played` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_competition`
--

/*!50001 DROP VIEW IF EXISTS `v_competition`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_competition` AS select sql_small_result `vc1`.`teamA_id` AS `team_id`,count(`vc1`.`played`) AS `games`,sum(`vc1`.`teamA_goals`) AS `scored`,sum(`vc1`.`teamB_goals`) AS `missed`,sum((case when (`vc1`.`teamA_goals` = `vc1`.`teamB_goals`) then 1 when (`vc1`.`teamA_goals` > `vc1`.`teamB_goals`) then 3 else 0 end)) AS `scores`,sum((`vc1`.`teamA_goals` > `vc1`.`teamB_goals`)) AS `wins`,sum((`vc1`.`teamA_goals` = `vc1`.`teamB_goals`)) AS `draws`,sum((`vc1`.`teamA_goals` < `vc1`.`teamB_goals`)) AS `defeats` from (select `c1`.`teamA_id` AS `teamA_id`,`c1`.`teamA_goals` AS `teamA_goals`,`c1`.`teamB_goals` AS `teamB_goals`,`c1`.`played` AS `played` from `test5`.`competition` `c1` union all select `c1`.`teamB_id` AS `teamB_id`,`c1`.`teamB_goals` AS `teamB_goals`,`c1`.`teamA_goals` AS `teamA_goals`,`c1`.`played` AS `played` from `test5`.`competition` `c1`) `vc1` group by `vc1`.`teamA_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_group`
--

/*!50001 DROP VIEW IF EXISTS `v_group`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_group` AS select sql_small_result sql_cache `c1`.`group_id` AS `group_id`,`c1`.`played` AS `played`,`t1`.`id` AS `teamA_id`,`t1`.`country` AS `teamA_country`,(case when isnull(`c1`.`played`) then NULL else `c1`.`teamA_goals` end) AS `teamA_goals`,(case when isnull(`c1`.`played`) then NULL else (`c1`.`teamA_goals` - `c1`.`teamB_goals`) end) AS `teamA_scored_missed`,`t2`.`id` AS `teamB_id`,`t2`.`country` AS `teamB_country`,(case when isnull(`c1`.`played`) then NULL else `c1`.`teamB_goals` end) AS `teamB_goals`,(case when isnull(`c1`.`played`) then NULL else (`c1`.`teamB_goals` - `c1`.`teamA_goals`) end) AS `teamB_scored_missed`,(case when isnull(`c1`.`played`) then NULL else `c1`.`teamA_scores` end) AS `teamA_scores`,(case when isnull(`c1`.`played`) then NULL else `c1`.`teamB_scores` end) AS `teamB_scores` from ((`competition` `c1` join `team` `t1` on((`c1`.`teamA_id` = `t1`.`id`))) join `team` `t2` on((`c1`.`teamB_id` = `t2`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_group_winner`
--

/*!50001 DROP VIEW IF EXISTS `v_group_winner`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_group_winner` AS select sql_small_result sql_cache `vg1`.`group_id` AS `group_id`,`vg1`.`teamA_country` AS `teamA_country`,`vg1`.`teamA_goals` AS `teamA_goals`,`vg1`.`teamA_id` AS `teamA_id`,`vg1`.`teamB_id` AS `teamB_id`,`vg1`.`teamB_country` AS `teamB_country`,`vg1`.`teamB_goals` AS `teamB_goals`,`vg1`.`teamA_scores` AS `teamA_scores`,`vg1`.`teamB_scores` AS `teamB_scores`,`vg1`.`teamA_scored_missed` AS `teamA_scored_missed`,`vg1`.`teamB_scored_missed` AS `teamB_scored_missed`,`vt1`.`scores` AS `teamA_scores_total`,`vt2`.`scores` AS `teamB_scores_total`,(case when isnull(`vg1`.`played`) then NULL when (`vg1`.`teamA_scores` > `vg1`.`teamB_scores`) then 'A' when (`vg1`.`teamA_scores` < `vg1`.`teamB_scores`) then 'B' when (`vg1`.`teamA_scored_missed` > `vg1`.`teamB_scored_missed`) then 'A' when (`vg1`.`teamA_scored_missed` < `vg1`.`teamB_scored_missed`) then 'B' when (`vt1`.`scores` > `vt2`.`scores`) then 'A' when (`vt1`.`scores` > `vt2`.`scores`) then 'B' when (`vt1`.`id` > `vt2`.`id`) then 'A' else 'B' end) AS `winner_letter` from ((`test5`.`v_group` `vg1` join `test5`.`v_team` `vt1` on((`vg1`.`teamA_id` = `vt1`.`team_id`))) join `test5`.`v_team` `vt2` on((`vg1`.`teamB_id` = `vt2`.`team_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_team`
--

/*!50001 DROP VIEW IF EXISTS `v_team`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_team` AS select sql_small_result `t1`.`id` AS `id`,`t1`.`country` AS `country`,`vth1`.`team_id` AS `team_id`,`vth1`.`games` AS `games`,`vth1`.`scored` AS `scored`,`vth1`.`missed` AS `missed`,(`vth1`.`scored` - `vth1`.`missed`) AS `scored_missed`,`vth1`.`scores` AS `scores`,`vth1`.`wins` AS `wins`,`vth1`.`draws` AS `draws`,`vth1`.`defeats` AS `defeats`,((1.0 + `vth1`.`scored`) / (1.0 + `vth1`.`missed`)) AS `attack_power` from (`test5`.`v_team_history` `vth1` join `test5`.`team` `t1` on((`vth1`.`team_id` = `t1`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_team_history`
--

/*!50001 DROP VIEW IF EXISTS `v_team_history`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_team_history` AS select sql_small_result `th1`.`team_id` AS `team_id`,(`th1`.`games` + coalesce(`vc1`.`games`,0)) AS `games`,(`th1`.`scored` + coalesce(`vc1`.`scored`,0)) AS `scored`,(`th1`.`missed` + coalesce(`vc1`.`missed`,0)) AS `missed`,(`th1`.`scores` + coalesce(`vc1`.`scores`,0)) AS `scores`,(`th1`.`wins` + coalesce(`vc1`.`wins`,0)) AS `wins`,(`th1`.`draws` + coalesce(`vc1`.`draws`,0)) AS `draws`,(`th1`.`defeats` + coalesce(`vc1`.`defeats`,0)) AS `defeats` from (`test5`.`team_history` `th1` left join `test5`.`v_competition` `vc1` on((`th1`.`team_id` = `vc1`.`team_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-12  6:55:36
