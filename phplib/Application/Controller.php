<?php
	/** класс для согласованного выполнения программ пакета
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/

	namespace Application ;

	/**
	* @subpackage Application\Controller контроллер
	*/
	class Controller extends Base {
		/**
		* Создание объекта
		* @param stdclass $creator ссылка на объект-создатель
		*/
		public function __construct( $creator = null ) {
			// вызов метода родителя
			parent::__construct( $creator ) ;

			/**
			* @var Application\Model\Championship $championship - объект модели чемпионата
			*/
			$this->championship = new Model\Championship( $this ) ;

			/**
			* @var Application\Model\Competition $competition - объект модели соревнования
			*/
			$this->competition = new Model\Competition( $this ) ;

			/**
			* @var Application\Model\Team $team - объект модели команды
			*/
			$this->team = new Model\Team( $this ) ;
		}

		/**
		* Получение значений HTTP-аргументов.
		* @param array $name имена аргументов HTTP-запроса
		* @return mixed
		* если передано только одно имя аргумента, то возвращается скаляр,
		* если передано много имён, то возвращает массив
		*/
		protected function __arg( ) {
			$request = &$this->creator->request ;
			$result = array( ) ;

			foreach ( func_get_args( ) as $name ) {
				if ( ! isset( $request[ $name ] ) ) {
					$result[] = null ;

					continue ;
				}

				$result[] = $request[ $name ] ;
			}

			if ( count( $result ) == 1 ) {
				return $result[ 0 ] ;
			}

			return $result ;
		}

		/**
		* Оформление ответа для вывода в формате JSON
		* @return array инструкции для дальнейшей обработки запроса
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		* result.data - данные для вывода в теле HTTP-ответа
		* result.passthru - выводить представление не внутри общего представления, а сразу
		* result.key - имя переменной для присвоения массива сообщений
		*/
		protected function __json( &$data ) {
			return array(
				'result' => array(
					'data' => $data
				) ,
				'view' => 'json' ,
				'headers' => array(
					array(
						$this->creator->config->http->header->default_name ,
						$this->creator->config->http->header->javascript
					)
				) ,
				'key' => 'message' ,
				'passthru' => true
			) ;
		}

		/**
		* Идентификаторы пользователя
		* @return array
		* string( 32 ) $session_id - идентификатор сессии
		* int $user_id  - идентификатор пользователя
		*/
		protected function __ids( ) {
			$session_id = $this->__arg( 'session_id' ) ;
			$user = $this->user->info( $session_id ) ;

			return array(
				$session_id ,
				$user[ 'id' ]
			) ;
		}

		/**
		* Главная страница
		* @return array инструкции для дальнейшей обработки запроса
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		*/
		public function indexAction( ) {
			return array(
				'view' => 'index' ,
				'result' => array( )
			) ;
		}

		/**
		* Список команд
		* @return array информация о командах
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		*/
		public function teamlistAction( ) {
			$result = $this->team->all( ) ;

			return $this->__json( $result ) ;
		}

		/**
		* Жеребьёвка
		* @return array of array - таблица жеребьёвки
		*/
		public function suggestAction( ) {
			$result = array(
				'championship_id' => $this->championship->create( ) ,
				'groups' => array( )
			) ;
			foreach ( $this->competition->suggest( $result[ 'championship_id' ] ) as $group_id ) {
				$result[ 'groups' ][ $group_id ] = $this->competition->group( $group_id ) ;
			}

			return $this->__json( $result ) ;
		}

		/**
		* розыгрыш
		* @return array of array - таблица розыгрыша
		*/
		public function playAction( ) {
			$round_id = $this->__arg( 'round_id' ) ;

			if ( ! in_array( $round_id , array( 1 , 2 ) ) ) {
				return null ;
			}

			$round_id = 'play' . $round_id ;
			$group_ids = $this->__arg( 'group_ids' ) ;
			$result = array(
				'championship_id' => $this->__arg( 'championship_id' ) ,
				'groups' => array( )
			) ;
			$group_id = $this->competition->$round_id( $group_ids ) ;
			$result[ 'groups' ][ $group_id ] = $this->competition->group( $group_id ) ;

			return $this->__json( $result ) ;
		}

		/**
		* история розыгрышей
		* @return array of array - таблица розыгрыша
		*/
		public function historyAction( ) {
			$championship_id = $this->__arg( 'championship_id' ) ;
			$result = $this->championship->history( $championship_id ) ;

			return $this->__json( $result ) ;
		}
	}
