<?php
	/** пакет моделей
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	namespace Application\Model ;

	/** класс модели соревнования
	* @subpackage \Application\Model модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class Championship extends \Application\Model {
		/**
		* создать чемпионат
		* @return char( 32 ) - идентификатор
		*/
		public function create( ) {
			$id = md5( uniqid( ) ) ;
			$entityName = $this->entityName( ) ;
			$sth = $this->dbh->prepare( '
INSERT INTO
	`championship`
SET
	`id` := :id ;
			' ) ;
			$sth->execute( array(
				'id' => $id
			) ) ;
			$sth->closeCursor( ) ;

			return $id ;
		}

		/**
		* История проведения соревнования
		* @param string( 32 ) $id - идентификатор чемпионата
		* @return array of array - дерево соревнований команд
		*/
		public function history( $championship_id ) {
			return $this->creator->competition->history( $championship_id ) ;
		}
	}