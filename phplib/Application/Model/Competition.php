<?php
	/** пакет моделей
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	namespace Application\Model ;

	/** класс модели соревнования
	* @subpackage \Application\Model модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class Competition extends \Application\Model {
		/**
		* Генерация идентификатора
		* @return char( 32 ) - идентификатор
		*/
		protected function id( ) {
			return md5( uniqid( ) ) ;
		}

		/**
		* Жеребьёвка
		* @param char( 32 ) $championship_id - идентификатор чемпионата
		* @return array of char( 32 ) - идентификаторы 
		*/
		public function suggest( $championship_id ) {
			$sel_sth = $this->dbh->prepare( '
SELECT SQL_SMALL_RESULT
	`t1`.`id` AS `team_id`
FROM
	`team` AS `t1`
LIMIT ' . $this->app->config->game->team_count . ' ;
			' ) ;
			$sel_sth->execute( ) ;
			$team_ids = array( ) ;

			while ( $team_id = $sel_sth->fetchColumn( ) ) {
				$team_ids[] = $team_id ;
			}
			$sel_sth->closeCursor( ) ;

			shuffle( $team_ids ) ;

			$ins_sth = $this->dbh->prepare( '
INSERT INTO
	`competition`
SET
	`championship_id` = :championship_id ,
	`group_id` = :group_id ,
	`teamA_id` = :teamA_id ,
	`teamB_id` = :teamB_id ;
			' ) ;
			$group_ids = array( ) ;

			for (
				$i = 0 ;
				( $teamA_id = array_shift( $team_ids ) ) && ( $teamB_id = array_shift( $team_ids ) ) ;
				$i += 2
			) {
				if ( $i % $this->app->config->game->group_volume == 0 ) {
					$group_id = $this->id( ) ;
					$group_ids[] = $group_id ;
				}

				$ins_sth->execute( array(
					'championship_id' => $championship_id ,
					'group_id' => $group_id ,
					'teamA_id' => $teamA_id ,
					'teamB_id' => $teamB_id
				) ) or die( print_r( $ins_sth , true ) ) ;
			}

			$ins_sth->closeCursor( ) ;

			return $group_ids ;
		}

		/**
		* первый раунд
		* @param array of char( 32 ) $group_ids - идентификатор группы
		* @return bool - успех
		*/
		public function play1( $group_ids ) {
			$sel_sth = $this->dbh->prepare( '
SELECT
	`c1`.`teamA_id` ,
	`c1`.`teamB_id`
FROM
	`competition` AS `c1`
WHERE
	( `c1`.`group_id` = :group_id ) ;
			' ) ;
			$upd_sth = $this->dbh->prepare( '
UPDATE
	`competition` AS `c1`
SET
	`c1`.`teamA_goals` := :teamA_goals ,
	`c1`.`teamB_goals` := :teamB_goals ,
	`c1`.`teamA_scores` := CASE
		WHEN ( :teamA_goals > :teamB_goals ) THEN :game_win
		WHEN ( :teamA_goals < :teamB_goals ) THEN :game_defeat
		ELSE :game_draw
	END ,
	`c1`.`teamB_scores` := CASE
		WHEN ( :teamA_goals < :teamB_goals ) THEN :game_win
		WHEN ( :teamA_goals > :teamB_goals ) THEN :game_defeat
		ELSE :game_draw
	END ,
	`c1`.`played` := current_timestamp( )
WHERE
	( `c1`.`group_id` = :group_id ) AND
	( `c1`.`teamA_id` = :teamA_id ) AND
	( `c1`.`teamB_id` = :teamB_id ) ;
			' ) ;

			foreach ( $group_ids as $i => $group_id ) {
				$sel_sth->execute( array(
					'group_id' => $group_id
				) ) ;

				while ( list( $teamA_id , $teamB_id ) = $sel_sth->fetch( \PDO::FETCH_NUM ) ) {
					$teamA = $this->creator->team->getByID( $teamA_id ) ;
					$teamB = $this->creator->team->getByID( $teamB_id ) ;

					$teamA_goals = $this->creator->team->goal( $teamA , $teamB ) ;
					$teamB_goals = $this->creator->team->goal( $teamB , $teamA ) ;

					$upd_sth->execute( array(
						'game_defeat' => $this->app->config->game->defeat ,
						'game_win' => $this->app->config->game->win ,
						'game_draw' => $this->app->config->game->draw ,
						'group_id' => $group_id ,
						'teamA_id' => $teamA_id ,
						'teamB_id' => $teamB_id ,
						'teamA_goals' => $teamA_goals ,
						'teamB_goals' => $teamB_goals
					) ) or die( print_r( $this->dbh->errorInfo( ) , true) ) ;
				}
			}
			$upd_sth->closeCursor( ) ;
			$sel_sth->closeCursor( ) ;

			$group_id_current = $this->play2( $group_ids ) ;

			return $group_id_current ;
		}

		/**
		* второй и следующие раунды
		* @param array of char( 32 ) $group_ids - идентификатор группы
		* @return bool - успех
		*/

		function play2( $group_ids ) {
			$group_id_current = $this->id( ) ;

			$ins_sth = $this->dbh->prepare( '
INSERT INTO
	`competition`(
		`championship_id` ,
		`parent_id` ,
		`group_id` ,
		`teamA_id` ,
		`teamB_id` ,
		`teamA_goals` ,
		`teamB_goals` ,
		`teamA_scores` ,
		`teamB_scores` ,
		`played`
	)
SELECT
	`c1`.`championship_id` ,
	`c1`.`group_id` AS `parent_id` ,
	:group_id AS `group_id` ,
	:teamA_id AS `teamA_id` ,
	:teamB_id AS `teamB_id` ,
	:teamA_goals AS `teamA_goals` ,
	:teamB_goals AS `teamB_goals` ,
	:teamA_scores + CASE
		WHEN ( :teamA_goals > :teamB_goals ) THEN :game_win
		WHEN ( :teamA_goals < :teamB_goals ) THEN :game_defeat
		ELSE :game_draw
	END AS `teamA_scores` ,
	:teamB_scores + CASE
		WHEN ( :teamA_goals < :teamB_goals ) THEN :game_win
		WHEN ( :teamA_goals > :teamB_goals ) THEN :game_defeat
		ELSE :game_draw
	END AS `teamB_scores` ,
	current_timestamp( ) AS `played`
FROM
	`competition` AS `c1`
WHERE
	( `c1`.`group_id` = :parent_id )
LIMIT 1 ;
			' ) ;

			foreach ( $group_ids as $i => $group_id ) {
				$winners = array( ) ;

				foreach ( $this->group( $group_id ) as $i => $teams ) {
					$winner = 'team' . $teams[ 'winner_letter' ] . '_' ;
					$winner_id = $teams[ $winner . 'id' ] ;
					$winners[] = array(
						'id' => $winner_id ,
						'scores' => $teams[ $winner . 'scores' ] ,
						'team' => $this->creator->team->getByID( $winner_id )
					) ;

					if ( ! ( $i % 2 ) ) {
						continue ;
					}

					$ins_sth->execute( array(
						'game_defeat' => $this->app->config->game->defeat ,
						'game_win' => $this->app->config->game->win ,
						'game_draw' => $this->app->config->game->draw ,
						'parent_id' => $group_id ,
						'group_id' => $group_id_current ,
						'teamA_id' => $winners[ 0 ][ 'id' ] ,
						'teamB_id' => $winners[ 1 ][ 'id' ] ,
						'teamA_scores' => $winners[ 0 ][ 'scores' ] ,
						'teamB_scores' => $winners[ 1 ][ 'scores' ] ,
						'teamA_goals' => $this->creator->team->goal( $winners[ 0 ][ 'team' ] , $winners[ 1 ][ 'team' ] ) ,
						'teamB_goals' => $this->creator->team->goal( $winners[ 1 ][ 'team' ] , $winners[ 0 ][ 'team' ] )
					) ) ;

					$winners = array( ) ;
				}
			}
			$ins_sth->closeCursor( ) ;

			return $group_id_current ;
		}

		/**
		* История проведения соревнования
		* @param string( 32 ) $id - идентификатор чемпионата
		* @return array of array - дерево соревнований команд
		*/
		public function history( $championship_id , $parent_id = null ) {
			$result = array(
				'groups' => array( )
			) ;
			$sql_args = array(
				'championship_id' => $championship_id
			) ;
			$sql = '
SELECT SQL_CACHE SQL_SMALL_RESULT
	`vc1`.`group_id` ,
	`vc1`.`played`
FROM
	`v_championship` AS `vc1`
WHERE
	( `vc1`.`championship_id` = :championship_id )
			' ;

			if ( is_null( $parent_id ) ) {
				$sql .= ' AND ( `vc1`.`parent_id` IS null ) ' ;
			} else {
				$sql_args[ 'parent_id' ] = $parent_id ;
				$sql .= ' AND ( `vc1`.`parent_id` = :parent_id ) ' ;
			}

			$sel1_sth = $this->dbh->prepare( $sql ) ;
			$sel1_sth->execute( $sql_args ) ;
			while ( list( $group_id , $played ) = $sel1_sth->fetch( \PDO::FETCH_NUM ) ) {
				$result[ 'groups' ][ $group_id ] = array(
					'teams' => $this->group( $group_id ) ,
					'child' => $this->history( $group_id ) ,
					'played' => $played
				) ;
			}
			$sel1_sth->closeCursor( ) ;

			return $result ;
		}

		/**
		* Список команд группы
		* @param string( 32 ) $group_id - идентификатор группы
		* @return array of array - список пар команд в группе
		*/
		public function group( $group_id ) {
			$sth = $this->dbh->prepare( '
SELECT SQL_SMALL_RESULT SQL_CACHE
	`vgw1`.`group_id` ,
	`vgw1`.`teamA_id` ,
	`vgw1`.`teamA_country` ,
	`vgw1`.`teamA_goals` ,
	`vgw1`.`teamB_id` ,
	`vgw1`.`teamB_country` ,
	`vgw1`.`teamB_goals` ,
	`vgw1`.`teamA_scores` ,
	`vgw1`.`teamB_scores` ,
	`vgw1`.`teamA_scored_missed` ,
	`vgw1`.`teamB_scored_missed` ,
	`vgw1`.`teamA_scores_total` ,
	`vgw1`.`teamB_scores_total` ,
	`vgw1`.`winner_letter`
FROM
	`v_group_winner` AS `vgw1`
WHERE
	( `vgw1`.`group_id` = :group_id ) ;
			' ) ;

			$sth->execute( array(
				'group_id' => $group_id
			) ) ;
			$results = $sth->fetchAll( \PDO::FETCH_ASSOC ) ;
			$sth->closeCursor( ) ;

			if ( empty( $results ) ) {
				return array( ) ;
			}

			return $results ;
		}
	}