<?php
	/** пакет моделей
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	namespace Application\Model ;

	/** класс модели команды
	* @subpackage \Application\Model модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class Team extends \Application\Model {
		/**
		* список команд
		* @return array - список команд
		*/
		public function all( ) {
			$sth = $this->dbh->prepare( '
SELECT
	`vt1`.`id`,
	`vt1`.`country`,
	`vt1`.`team_id`,
	`vt1`.`games`,
	`vt1`.`scored`,
	`vt1`.`missed`,
	`vt1`.`scores`,
	`vt1`.`wins`,
	`vt1`.`draws`,
	`vt1`.`defeats`,
	`vt1`.`attack_power`
FROM
	`v_team` AS `vt1`
ORDER BY
	`vt1`.`attack_power` DESC ;
			' ) ;
			$sth->execute( ) ;
			$result = $sth->fetchAll( \PDO::FETCH_OBJ ) ;
			$sth->closeCursor( ) ;

			return $result ;
		}

		/**
		* Информация о команде
		* @param int $team_id - идентификатор о команды
		* @return stdClass - информация о команде
		*/
		public function getByID( $team_id ) {
			$sel_sth = $this->dbh->prepare( '
SELECT SQL_SMALL_RESULT
	`vt1`.`id`,
	`vt1`.`country`,
	`vt1`.`team_id`,
	`vt1`.`games`,
	`vt1`.`scored`,
	`vt1`.`missed`,
	`vt1`.`scores`,
	`vt1`.`wins`,
	`vt1`.`draws`,
	`vt1`.`defeats`,
	`vt1`.`attack_power`
FROM
	`v_team` AS `vt1`
WHERE
	( `vt1`.`id` = :team_id )
LIMIT 1 ;
			' ) ;
			$sel_sth->execute( array(
				'team_id' => $team_id
			) ) ;
			$team = $sel_sth->fetch( \PDO::FETCH_OBJ ) ;
			$sel_sth->closeCursor( ) ;

			return $team ;
		}


		/**
		* количество голов, забитых командой "A" команде "B"
		* @param stdClass $teamA - команда "A"
		* @param stdClass $teamB - команда "B"
		* @return int - количество забитых голов команды "A"
		*/
		public function goal( $teamA , $teamB ) {
			return ceil( ( 1.0 + $teamA->attack_power ) / ( 1.0 + $teamB->attack_power ) *
				rand( 0 , $this->app->config->game->max_goals ) ) ;
		}
	}