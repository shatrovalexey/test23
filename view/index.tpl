<form class="main-form" id="top">
<table border="1" cellspacing="0" cellpadding="4" width="100%" class="command-list-table nod">
	<caption>
		<h2>Список команд</h2>
		<div>Список обновляется автоматически без перезагрузки страницы при каждом нажатии очередной кнопки.</div>
		<label class="label-ghzerebjovka">
			<span>запустить жеребьёвку</span>
			<input type="button" value="&rarr;" class="button-ghzerebjovka">
		</label>
	</caption>
	<thead>
		<tr>
			<th>№</th>
			<th>&nbsp;</th>
			<th title="игр">и</th>
			<th title="побед">в</th>
			<th title="ничей">н</th>
			<th title="поражений">п</th>
			<th title="мячей забито - пропущено">мячи</th>
			<th title="очков">оч</th>
			<th title="сила атаки">сила атаки</th>
		</tr>
	</thead>
	<tbody class="team-list">
		<tr class="nod">
			<td data-col="id" class="num"></td>
			<td data-col="country" class="team-title"></td>
			<td data-col="games" class="num"></td>
			<td data-col="wins" class="num"></td>
			<td data-col="draws" class="num"></td>
			<td data-col="defeats" class="num"></td>
			<td class="num">
				<span data-col="scored"></span> -
				<span data-col="missed"></span>
			</td>
			<td data-col="scores" class="num"></td>
			<td data-col="attack_power" class="num"></td>
		</tr>
	</tbody>
</table>

	<div class="championship nod" id="championship">
		<h2>
			<a href="#top" class="top-href">&uarr;</a>
			<span>Чемпионат</span>
			<span class="championship-id num"></span>
			<div class="both"></div>
		</h2>
		<div>
			<div>
				<span>После жеребьёвки голов с очками ещё нет.</span>
			</div>
			<label class="label-group-run">
				<span>Запустить групповые матчи</span>
				<input type="button" value="&rarr;" class="button-group-run">
			</label>
			<div>
				<p>нажимайте кнопку для выполнения дальнейших розыгрышей.
			</div>
		</div>
		<table class="group nod" width="100%" border="1">
			<caption>
				<h3>
					<span>Группа</span>
					<span data-col="group_id" class="num"></span>
					<span data-col="played" class="num"></span>
					<a href="#top" class="top-href">&uarr;</a>
					<div class="both"></div>
				</h3>
			</caption>
			<thead>
				<th class="team-title">команда "A"</th>
				<th>голы</th>
				<th>очки</th>

				<th class="team-title">команда "B"</th>
				<th>голы</th>
				<th>очки</th>
			</thead>
			<tbody>
				<tr>
					<td data-col="teamA_country" data-team="A"></td>
					<td data-col="teamA_goals" data-team="A"></td>
					<td data-col="teamA_scores" data-team="A"></td>

					<td data-col="teamB_country" data-team="B"></td>
					<td data-col="teamB_goals" data-team="B"></td>
					<td data-col="teamB_scores" data-team="B"></td>
				</tr>
			</tbody>
		</table>
	</div>
</form>