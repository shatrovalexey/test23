jQuery( function( ) {
	var $team_list_update = function( ) {
		jQuery.ajax( {
			"url" : "/teamlist" ,
			"type" : "POST" ,
			"dataType" : "json" ,
			"success" : function( $data ) {
				var $list = jQuery( ".team-list" ) ;
				var $tr = $list.find( "tr.nod" ).clone( true ) ;

				$list.empty( ) ;

				jQuery( $data.data ).each( function( $i , $team ) {
					var $trCurrent = $tr.clone( true ).removeClass( "nod" ) ;

					if ( $i < 32 ) {
						$trCurrent.addClass( "team-selected" ) ;
					}

					$trCurrent.find( "*[data-col]" ).each( function( $col ) {
						var $self = jQuery( this ) ;
						var $key = $self.data( "col" ) ;

						if ( ! ( $key in $team ) ) {
							return true ;
						}

						$self.text( $team[ $key ] ) ;

						return true ;
					} ) ;
					$list.append( $trCurrent ) ;
				} ) ;

				$list.append( $tr ) ;
				jQuery( ".command-list-table" ).removeClass( "nod" ) ;
			}
		} ) ;
	} ;

	$team_list_update.call( this ) ;

	var $group_ids ;
	var $championship_id ;

	var $group_list_update = function( $data ) {
		var $championship = jQuery( ".championship" ) ;
		$championship_id = $data.data.championship_id ;

		$championship.find( ".championship-id" ).text( $championship_id ) ;
		$championship.find( ".group" ).addClass( "remove" ) ;

		var $group = $championship.find( ".group:first" ) ;
		$group_ids = [ ] ;

		for ( $key in $data.data.groups ) {
			$group_ids.push( $key ) ;

			var $groupData = $data.data.groups[ $key ] ;
			var $groupCurrent = $group.clone( true ).removeClass( "nod" ).removeClass( "remove" ) ;

			$groupCurrent.find( "*[data-col=group_id]" ).text( $key ) ;
			$groupCurrent.attr( "data-key" , $key ) ;

			var $tr = $groupCurrent.find( "tbody tr:first" ) ;

			$groupCurrent.find( "tbody tr" ).addClass( "remove" ) ;

			jQuery( $groupData ).each( function( $j , $groupItem ) {
				var $trCurrent = $tr.clone( true ).removeClass( "remove" ) ;

				$trCurrent.find( "*[data-col]" ).each( function( ) {
					var $self = jQuery( this ) ;
					var $key = $self.data( "col" ) ;

					if ( ! ( $key in $groupItem ) ) {
						return true ;
					}

					var $team_name = $self.data( "team" ) ;

					if ( 'winner_letter' in $groupItem ) {
						$self.text( $groupItem[ $key ] ).
							attr( "class" , "" ).
							addClass( "team-winner-" + $team_name + $groupItem.winner_letter ) ;
					}

					return true ;
				} ) ;

				$tr.after( $trCurrent ) ;
			} ) ;

			$tr.remove( ) ;
			$group.after( $groupCurrent ) ;
		}

		$championship.find( ".remove" ).remove( ) ;
		$championship.removeClass( "nod" ) ;

		$team_list_update.call( this ) ;
	} ;

	jQuery( ".button-ghzerebjovka" ).on( "click" , function( ) {
		jQuery( this ).parent( "label:first" ).addClass( "nod" ) ;

		jQuery.ajax( {
			"url" : "/ghzerebjovka" ,
			"type" : "POST" ,
			"dataType" : "json" ,
			"context" : this ,
			"success" : function( $data ) {
				var $result = $group_list_update.call( this , $data ) ;

				location.hash = "#championship" ;

				return $result ;
			}
		} ) ;

		return false ;
	} ) ;

	jQuery( ".button-group-run" ).on( "click" , function( ) {
		var $self = jQuery( this ) ;
		var $round_id = $self.data( "round_id" ) ? 2 : 1 ;

		$self.prop( "disabled" , true ) ;

		jQuery.ajax( {
			"url" : "/play" ,
			"type" : "POST" ,
			"data" : {
				"round_id" : $round_id ,
				"championship_id" : $championship_id ,
				"group_ids" : $group_ids
			} ,
			"dataType" : "json" ,
			"context" : this ,
			"success" : function( $data ) {
				$self.attr( "data-round_id" , 1 ).prop( "disabled" , false ) ;

				var $result = $group_list_update.call( this , $data ) ;

				var $groups = jQuery( ".group" ) ;

				if ( ( $groups.length <= 1 ) && ( $groups.find( "tbody tr" ).length <= 1 ) ) {
					$self.parents( "label:first" ).addClass( "nod" ).next( ).addClass( "nod" ) ;
				}

				return $result ;
			}
		} ) ;

		return false ;
	} ) ;

	location.hash = "#top" ;
} ) ;